# create a lambda function to create s3 bucket
import boto3
def create_s3_bucket(bucket_name):
    s3_client = boto3.client('s3')
    s3_client.create_bucket(Bucket=bucket_name)